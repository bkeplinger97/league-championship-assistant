use std::io::{BufReader,BufRead};
use std::fs::File;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::vec::Vec;

fn main() {
    let champ_test: Vec<String> = ["Jhin".to_string(),"Xayah".to_string(),"Caitlyn".to_string()].to_vec();
    match parse_champions() {
        // If we caught an error
        Err(_err_message) => println!("Error"),
        // If we didn't catch an error, we get a map back
        Ok(_map) => {
            match _map {
                (map_1,map_2) => recommend_without_matchup(map_1,map_2,champ_test),
            };
        },
    }
}

// Potentially use later
/*
fn print_map(map: i8) -> (){

    for (key,value) in map {
        println!("Key: {}, Value: {}",key,value);
    }

}
*/

// Parses the file champions to build out a map of what classifiers we consider champions to have
// If a champion undergoes a rework, we can reclassify them in that file and rerun this command
// null -> Result(null,HashMap<String,Vec<String>>)
fn parse_champions() -> std::io::Result<(HashMap<String,Vec<String>>,HashMap<String,Vec<String>>)> {
    let f = File::open("src/champions")?; // Query to open the file
    let f = BufReader::new(f); // Set up a buffered reader
    // Mutable attribute list to add our attributes to each champion
    let mut attributes_list = HashMap::new();
    let mut champ_att_list = HashMap::new();
    let att_start: Vec<&str> = vec!["Mage","Assassin","Brawler","Marksman","Tank","Support",
        "Specialist","Back Line","Front Line","Poke","Dive","Burst","Physical Damage","Magic Damage",
        "Hybrid Damage","Health Sustain","True Damage","Sustain Damage","Mana Sustain","Safe","All-In",
        "Low Mobility","High Mobility","Early Game","Early to Mid Game","Mid Game","Mid to Late Game","Late Game","Team Healing",
        "Shielding","Invulnerability","Revival","Confusion","Scouting","Stat Stealing","Chase","Reset",
        "Team Mobility","Displacement","Taunt","Speed Up","Slow","Polymorph","Pull","Disengage",
        "Blind","Charm","Knockup","Stun","Fear","Silence","Objective Security","Spell Shield",
        "Versatility","Terrain Creation","Silence","Invisibility"];


    // Initialize our attribute list so we don't need to do so much checking later
    for x in att_start {
        let insertvec: Vec<String> = Vec::new();
        attributes_list.insert(x.to_string(),insertvec);
    }

    // Go through the file
    for line in f.lines() {
        // Take the line and strip off the champion name, delimited by the ':'
        let linename = line.unwrap(); // TODO input type of linename
        let name: Vec<_> = linename.split_terminator(":").collect();

        // Now get the attributes, if we have at least 2 elements we know we have attributes
        // Uncomment next line when champion file is filled out (recomment line after)
        //if name.len() > 0 {
        if name.len() > 1 {
            // Split the attributes based on a comma
            let list_of_attributes: Vec<_> = name[1].split_terminator(',').collect();
            champ_att_list.insert(name[0].to_string(),Vec::new());
            // Add the champion to the attribute
            for att in list_of_attributes {
                // See if our item is within the above spec'd list or not
                match attributes_list.entry(att.to_string()) {
                    Entry::Vacant(_e) => {
                        if att.to_string().len() > 11 && att[0..11].to_string() == "Difficulty=" {
                            //println!("Found something I don't recognize {}",att)
                        }
                    }, // Do nothing
                    Entry::Occupied(mut e) => {
                        e.get_mut().push(name[0].to_string());
                    },
                }
                // Map the champions to the attribute to do some reverse searching later
                match champ_att_list.get_mut(&name[0].to_string()) {
                    Some(item) => item.push(att.to_string()),
                    None => (),//println!("Shit"),
                }
            }
        }
        // Otherwise there was nothing on the line and we ignore it.
        else {

        }
    }

    // Note this is not deterministic, keys may be read in in any order
    // Print out all the attributes and their vectors
    /*
    for (key,value) in &attributes_list {
        print!("{}: ",key);
        for x in value {
            print!("{} ",x.to_string());
        }
        print!("\n\n");
    }
    */
    // Exit nicely
    Ok((attributes_list,champ_att_list))
}

// (list of all attributes->champions, list of champions and their attributes, vector of the
// champions people play), returns a vector of champions as a recommendations

/* Takes into account only the features of the champions the user plays and that's it, no matchup
 * information. This would be for first picking and would be faster than looking at matchups
 */
pub fn recommend_without_matchup(attributes_list: HashMap<String,Vec<String>>,
        mut champ_att_list: HashMap<String,Vec<String>>,player_champion_list: Vec<String>)
        -> Vec<String> {
    let mut common_att_list:HashMap<String,i8> = HashMap::new();
    let champ_recommendation_list: Vec<String> = Vec::new();
    // Start by taking the champions the player plays, searching for the common attributes between
    // them
    for champion in &player_champion_list {
        // Check that the champion is within the map of champions->attributes
        match champ_att_list.get_mut(&champion.to_string()) {
            // If the champion is valid, it returns that champions list of attributes
            Some(list) => {
                // Now we want to add all of those attributes to the common_att_list to see what
                // attributes the champion picks have in common
                for attribute in list {
                    if !common_att_list.contains_key(&attribute.to_string()) {
                        common_att_list.insert(attribute.to_string(),1);
                    }
                    else {
                        *common_att_list.get_mut(&attribute.to_string()).unwrap() += 1;
                    }
                }
            },
            None => println!("Shit")
        }
    }

    // Sort our list
    let mut count_vec: Vec<(&String, &i8)> = common_att_list.iter().collect();
    count_vec.sort_by(|a, b| b.1.cmp(&a.1));

    // TODO Specify this as debug only?
/*
    for (attribute,number) in &common_att_list {
        println!("{} had {} entries",attribute.to_string(),number);
    }
*/

    // Now look for similar champions within attributes_list, this is a long ass operation so it
    // may be worth it to use the big class groupings (marksman, mage, etc...), this would mean we
    // could provide lower latency but this is something we should address later. Get it working,
    // then make it better
    let _highest_count: i8 = 0;
    let _highest_count_champion: String = "".to_string();
    let mut hashmap : HashMap<&str,i8>= HashMap::new(); // Map champions to commonality count
    let _most_common: i8 = 0;

    // So now we want the most common attributes to be looked at first, as it seems like those
    // would lead to a similar champion for the player
    // For now just do the ones with count higher than 1
    for (given_att,_count) in count_vec {
        if _count > &1 {
            match attributes_list.get(given_att) {
                Some(list) => {
                    for champion in list {
                        match hashmap.contains_key::<str>(champion) {
                            true => {
                                *hashmap.get_mut::<str>(champion).unwrap() += 1;
                            },
                            false => {
                                hashmap.insert(champion,1);
                            }
                        }
                    }
                },
                None => {
                    ()
                }
            }
        }
    }

    // Sort our list
    let mut champ_vec: Vec<(&&str, &i8)> = hashmap.iter().collect();
    champ_vec.sort_by(|a, b| b.1.cmp(a.1));

    println!("Printing out recommended champions!");
    for (champion,count) in champ_vec {
        if count >= &3 && !player_champion_list.contains(&champion.to_string()){
            println!("Recommended champion: {} {}",champion,count);
        }
    }

    // Return our recommendations k
    champ_recommendation_list
}
