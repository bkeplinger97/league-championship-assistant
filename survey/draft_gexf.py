

# Go through the gexf file, see where we need to read and write to the gexf,
# then fill out the graph
def read_gexf(read_file_name,write_file_name):
    rfile = open('champcopy',"r")
    wfile = open(write_file_name,"w")
    wfile.write('<?xml version="1.0" encoding="UTF-8"?>\n<gexf xmlns="http://www.gexf.net/1.2draft" version="1.2">\n<meta lastmodifieddate="%s">\n<creator>Brandon Keplinger</creator>\n<description>A mapping of Primary to Seconday and Tertiary picks</description>\n</meta>\n<graph mode="static" defaultedgetype="directed">\n<nodes>\n')
    write_nodes = False
    write_edges = False
    # Positional xml dictionary to say what champion has which ID
    xml_dict = {}
    xml_dict["Dodge"] = 0
    wfile.write('<node id="0" label="Dodge" />\n')
    xml_dict["Counter"] = 1
    wfile.write('<node id="1" label="Counter" />\n')
    xml_dict["Int"] = 2
    wfile.write('<node id="2" label="Int" />\n')
    xml_dict["Troll"] = 3
    wfile.write('<node id="3" label="Troll" />\n')
    xml_dict["Swap"] = 4
    wfile.write('<node id="4" label="Swap" />\n')
    count = 5
    for rline in rfile:
        rline = rline.rstrip()
        wfile.write('<node id="%d" label="%s" />\n'%(count,rline))
        xml_dict[rline] = count
        count += 1
        #wfile.write('<node id="%d" label="%s%s" />\n'%(count,rline,'_secondary'))
        #count += 1
        #wfile.write('<node id="%d" label="%s%s" />\n'%(count,rline,'_tertiary'))
        #count += 1
    wfile.write('</nodes>\n<edges>\n')
    rfile.close()
    rfile = open('results/Connections','r')
    count = 0
    for rline in rfile:
        rline = rline.rstrip()
        if 'Secondaries' in rline or 'Tertiaries' in rline:
            line_split = rline.rstrip().split('\t')
            type = line_split[1]
            primary_champ = line_split[0]
        elif rline != "":
            line_split = rline.split('\t')
            champ_name = line_split[2]
            champ_times = line_split[1]
            wfile.write('<edge id="%d" source="%d" target="%d" type="directed" label="%s"/>\n'%(count,xml_dict[primary_champ],xml_dict[champ_name],type))
            count += 1
    wfile.write('</edges>\n</graph>\n</gexf>')
    wfile.close()
    return

def main():
    read_file_name = "results/hello_world.gexf"
    write_file_name = "results/map.gexf"
    read_gexf(read_file_name,write_file_name)

if __name__ == "__main__":
    main()
