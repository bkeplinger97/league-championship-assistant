import pprint

import praw

file = open('subredditcount/subredditcount','w')

champ_subreddits = {'Aatrox':'aatroxmains','Ahri':'ahrimains','Akali':'akalimains','Alistar':'alistarmains','Amumu':'amumumains',
    'Anivia':'anivia','Annie':'anniemains','Aphelios':'apheliosmains','Ashe':'ashemains','Aurelion Sol':'aurelion_sol_mains',
    'Azir':'azirmains','Bard':'bardmains','Blitzcrank':'blitzcrankmains','Brand':'brandmains','Braum':'braummains','Caitlyn':'caitlynmains',
    'Camille':'camillemains','Cassiopeia':'Cassiopeiamains','Cho\'Gath':'chogathmains','Corki':'corkimains','Darius':'Dariusmains',
    'Diana':'dianamains','Dr. Mundo':'drmundomains','Draven':'draven','Ekko':'ekkomains','Elise':'elisemains','Evelynn':'evelynnmains',
    'Ezreal':'ezrealmains','Fiddlesticks':'Fiddlesticksmains','Fiora':'fioramains','Fizz':'fizzmains','Galio':'galiomains','Gangplank':'Gangplankmains',
    'Garen':'garenmains','Gnar':'gnarmains','Gragas':'gragasmains','Graves':'gravesmains','Hecarim':'hecarimmains','Heimerdinger':'heimerdingermains',
    'Illaoi':'illaoi','Irelia':'Ireliamains','Ivern':'ivernmains','Janna':'janna','Jarvan IV':'jarvanivmains','Jax':'jaxmains','Jayce':'jaycemains',
    'Jhin':'Jhinmains','Jinx':'leagueofjinx','Kai\'Sa':'kaisamains','Kalista':'kalistamains','Kindred':'kindred','Karma':'Karmamains',
    'Karthus':'Karthusmains','Kassadin':'kassadinmains','Katarina':'katarinamains','Kayle':'kaylemains','Kayn':'Kaynmains','Kennen':'kennenmains',
    'Kled':'Kledmains','Kog\'Maw':'kog_mains','LeBlanc':'leblancmains','Lee Sin':'leesinmains','Leona':'leonamains','Lissandra':'lissandramains',
    'Lucian':'lucianmains','Lulu':'lulumains','Lux':'Lux','Malphite':'Malphitemains','Malzahar':'Malzaharmains','Maokai':'maokaimains',
    'Master Yi':'masteryimains','Miss Fortune':'missfortunemains','Mordekaiser':'mordekaisermains','Morgana':'morganamains','Nami':'namimains',
    'Nasus':'nasusmains','Nautilus':'nautilusmains','Neeko':'neekomains','Nidalee':'Nidaleemains','Nocturne':'nocturnemains',
    'Nunu & Willump':'nunumains','Olaf':'olafmains','Orianna':'oriannamains','Ornn':'ornnmains','Pantheon':'pantheonmains','Poppy':'poppymains',
    'Pyke':'pykemains','Qiyana':'Qiyanamains','Rakan':'rakanmains','Rammus':'rammusmains','Rek\'Sai':'reksaimains','Renekton':'renektonmains',
    'Rengar':'rengarmains','Riven':'Rivenmains','Rumble':'Rumblemains','Ryze':'ryzemains','Sejuani':'sejuanimains',
    'Shaco':'Shacomains','Shyvana':'shyvanamains','Singed':'singedmains','Sion':'dirtysionmains','Sivir':'sivir',
    'Skarner':'skarnermains','Sona':'sonamains','Soraka':'sorakamains','Swain':'swainmains','Sylas':'sylasmains','Syndra':'syndramains',
    'Tahm Kench':'tahmkenchmains','Taliyah':'taliyahmains','Talon':'talonmains','Taric':'taricmains','Teemo':'teemotalk','Thresh':'threshmains',
    'Tristana':'Tristanamains','Trundle':'trundlemains','Tryndamere':'tryndameremains','Twisted Fate':'twistedfatemains','Twitch':'twitchmains',
    'Udyr':'udyrmains','Urgot':'urgotmains','Varus':'varusmains','Vayne':'Vaynemains','Veigar':'veigarmains','Vel\'Koz':'velkoz','Vi':'Vimains',
    'Viktor':'viktormains','Vladimir':'vladimirmains','Volibear':'volibearmains','Warwick':'warwickmains','Wukong':'wukongmains',
    'Xayah':'xayahmains','Xerath':'xerathmains','Xin Zhao':'xinzhaomains','Yasuo':'Yasuomains','Yorick':'yorickmains','Yuumi':'yumimains',
    'Zac':'thesecretweapon','Zed':'zedmains','Ziggs':'ziggsmains','Zilean':'zileanmains','Zoe':'zoemains','Zyra':'Zyramains'}

r = praw.Reddit(client_id='cLWEFErMNmfVVw',
                     client_secret='3DoWTDEPGArFFfJpvko_o-EzuB4',
                     user_agent='Windows:LeagueNewPlayerTool:001 (by u/fang152)')
for champion,subreddit in champ_subreddits.items():
    s = r.subreddit(subreddit)
    file.write('%s\t%d\n'%(champion,s.subscribers))
    #pprint.pprint(s.subscribers)
