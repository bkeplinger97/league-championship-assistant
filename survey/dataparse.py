import csv
from datetime import date
import requests
import time
import math
import struct
import statistics
import pprint
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

REGION_LIST = [
    'North America',
    'EU-West',
    'EU-Nordic-East',
    'Korea',
    'China',
    'Russia',
    'Brazil',
    'Oceania',
    'Latin America North',
    'Latin America South',
    'Turkey',
    'Garena',
    'Japan',
]

# Takes a csv file opened with read write capability and a list of acceptable
# champion names, returns the same csv file with abnormalities removed
# (all columns ints, commas not fucking everything up)
def check_file(championList,csvreader):
    userlist = {}
    # Go through every item in the table
    xcount = 0
    ycount = 0
    for row in csvreader:
        if ycount == 0:
            ycount = 1
            continue
        xcount = 0
        for column in row:
            # First we check the given name against a dictionary of usernames
            # and their regions, checking if we've already seen this username.
            #      If we have and the regions are the same we fully erase this
            # line (Don't add it to csvWriter)
            #      If we have seen the name but the regions are different, we
            # append that to the dictionary entries list
            #      If we haven't seen this person, we give them an empty list
            if xcount == 1:
                # New player
                if userlist.get(column) is None:
                    userlist[column] = []
                    userlist[column].append(row[xcount + 1])
                # Seen this player, but now we check whether we've seen for the
                # same region or not
                else:
                    # If the region is in our region list
                    if row[xcount + 1] in REGION_LIST:
                        # If we have a region difference
                        if row[xcount + 1] not in userlist[column]:
                            userlist[column].append(row[xcount + 1])
                        # Same data twice, we break to avoid doing more work
                        # (only breaks inner loop)
                        elif row[xcount + 1] in userlist[column] and row[xcount] != "":
                            print("Duplicate summoner name on line: %d"%(ycount + 1))
                            break
                    else:
                        print("Region not recognized on line: %s with title: %s"%(ycount,row[xcount+1]))
            # Check the champion name and make sure it's legit (use caps for
            # capitalization errors) (renekton versus Renekton)
            elif xcount == 3 or xcount == 5 or xcount == 7:
                if row[xcount].rstrip() not in championList and row[xcount] != "":
                    #print("Champion not recognized on line: %d with name %s"%(ycount,row[xcount]))
                    break
            xcount += 1
        ycount += 1
    #print("Finished going through the file with %d lines"%(ycount,))

# Layout of the file:
# 0 : Timestamp
# 1 : Name
# 2 : Region
# 3 : Primary Champ
# 4 : Primary Role
# 5 : Secondary Champ
# 6 : Secondary Role
# 7 : Tertiary Champ
# 8 : Tertiary Role
# 9 : Influencing of champion pick
# 10 : Meta player or no?
# 11 : Playrate?
# 12 : Preferred game mode
# 13 : Rank?
# 14 : Aggression level?
# 15 : Aggression explanation
# 16 : Additional comments

# Parse the region answer through the survey, convert them to their stub names
# that are used to access the riot games api
def parse_region(region_string):
    regionstring = ''
    if region_string == 'North America':
        regionstring = 'na1'
    elif region_string == 'EU-Nordic-East':
        regionstring = 'eun1'
    elif region_string == 'EU-West':
        regionstring = 'euw1'
    elif region_string == 'Korea':
        regionstring = 'kr'
    elif region_string == 'Russia':
        regionstring = 'ru'
    elif region_string == 'Brazil':
        regionstring = 'br1'
    elif region_string == 'Latin America North':
        regionstring = 'la1'
    elif region_string == 'Latin America South':
        regionstring = 'la2'
    elif region_string == 'Oceania':
        regionstring = 'oc1'
    elif region_string == 'Turkey':
        regionstring = 'tr1'
    elif region_string == 'Japan':
        regionstring = 'jp1'
    return regionstring

# Make and send the get request to check that the summoner exists
def make_request(region,username,key):
    newreq = 'https://%s.api.riotgames.com/lol/summoner/v4/summoners/by-name/%s?%s'%(region,username,key)
    r = requests.get(newreq)
    return r

# Write out the highest playrate champion in each rank from our survey, we use
# this for both normalized and non-normalized data points
def find_most(map,filename):
    for rank in map.keys():
        newlist = []
        highestnum = 0
        rankcount = 0
        for champ in map[rank].keys():
            if map[rank][champ] > highestnum:
                highestnum = map[rank][champ]
                newlist = [champ]
            elif map[rank][champ] == highestnum:
                newlist.append(champ)
            rankcount += map[rank][champ]
            #print('Name: %s %d'%(champ,map[rank][champ]))
        file_name = 'results/%s'%filename
        file = open(file_name,'a+')
        file.write('In %s, '%(rank,))
        for (count,x) in enumerate(newlist):
            if len(newlist) == 1 or len(newlist) == 2:
                file.write('%s '%(x,))
            elif count + 1 != len(newlist):
                file.write('%s, '%(x,))
            else:
                file.write('and %s '%(x,))
        if len(newlist) > 1:
            file.write('had the highest number of ranked plays at %d plays each out of %d\n'%(highestnum,rankcount))
        else:
            file.write('had the highest number of ranked plays at %d out of %d\n'%(highestnum,rankcount))
        #print('In %s, %s had the highest number of ranked plays at %d out of %d'%(rank,x,highestnum,rankcount))

# Normalize data based on subreddit density
def normalizedata(map):
    file = open('subredditcount/subredditcount','r')
    total = 0
    count = 0
    champion = ''
    size = 0
    for line in file:
        line_list = line.rstrip().split('\t')
        champion = line_list[0]
        size = int(line_list[1])
        total += size
        count += 1
    average = total/count
    file.close()
    file = open('subredditcount/subredditcount','r')
    for line in file:
        for rank in map.keys():

            line_list = line.rstrip().split('\t')
            champion = line_list[0]
            size = int(line_list[1])
            if champion in map[rank].keys():
                ratio: float = average / size
                map[rank][champion] = map[rank][champion] * ratio
    return map

# Perform z-score normalization on the given map
def z_score_normalizing(map):
    file = open('subredditcount/subredditcount','r')
    total = 0
    champion = ''
    size = 0
    count = 0
    sr_list = []
    for line in file:
        line_list = line.rstrip().split('\t')
        size = int(line_list[1])
        total += size
        count += 1
        sr_list.append(size)
    average = total/count

    std_dev = statistics.stdev(sr_list)

    z_normalized = {}
    file.seek(0)
    for line in file:
        line_list = line.rstrip().split('\t')
        name = line_list[0]
        size = int(line_list[1])
        z_normalized[name] = (size - average) / std_dev
    file.close()

    return z_normalized

# We Define the Larry Ratio in honor of Larry Herman, Instructor at the
# University of Maryland who taught with the strength of many
# <animal of the semester>s
def find_larry_ratio(rankmap,champ_subreddit_map):
    larry_ratio = {}
    for rank in rankmap:
        larry_ratio[rank] = {}
        for champion,count in rankmap[rank].items():
            larry_ratio[rank][champion] = count / champ_subreddit_map[champion]
    return larry_ratio

def parse_rank(rank_string,rankmap,champ_name):
    rank_arr = ['Iron','Bronze','Silver','Gold','Platinum','Diamond','Master','Grandmaster','Challenger']
    for rank in rank_arr:
        if rank in rank_string or rank == rank_string:
            if champ_name not in rankmap[rank].keys():
                rankmap[rank][champ_name] = 1
            else:
                rankmap[rank][champ_name] += 1
    return rankmap

# Remove the words from the map that aren't important (A, is, or, etc...)
def removeFiller(map,champs):
    stop_words = set(stopwords.words('english'))
    for word in stop_words:
        if word in map.keys():
            del map[word]
    remove_list = ['Like','What','When','Where','They','With','Play','Pick',
    'Have','Their','Them','Your','Well','Just','Another','Also','Other','Want',
    'More','Will','Then','Based','Bring','Above','That']
    for word in remove_list:
        if word in map.keys():
            del map[word]
    for key in champs:
        if key in map.keys():
            del map[key]

def remove_numbers(word):
    return word.replace('1','').replace('2','').replace('3','').replace('4','').replace('5','').replace('6','').replace('7','').replace('8','').replace('9','').replace('0','')

def consolidate_entries(map):
    suffixes = ["ing","ty",'s','ance', 'ancy', 'ence', 'ency','ion', 'sion', 'tion','ism','ity','logy','ment','ness','ship','']

# Output our various statistics, we find in this:
# Rank mapping to champions picked (as primary)
# Counts of champions picked as primary, secondary, and tertiary
# Map of champion picks to average aggression levels
def pullStatistics(championList,csvreader):
    championmap = {} # Number of times champion picked as primary
    relationshipmap = {} # Maps between primary, seconday, and tertiary picks
    secondarychampcount = {} # Number of times champion picked as secondary
    tertiarychampcount = {} # Number of times champion picked as tertiary
    aggressionmap = {} # Champion to average aggression
    # rankmap keeps track of the amount of champion picks
    rankmap = {'Iron':{},'Bronze':{},'Silver':{},'Gold':{},'Platinum':{},
        'Diamond':{},'Master':{},'Grandmaster':{},'Challenger':{}}
    ycount = 0 # Keep track of the total line count
    totalprimarycount = 0 # Count how many champions total were picked first
    totalsecondarycount = 0 # Count how many were secondaries were picked
    totaltertiarycount = 0 # Count how many were tertiaries were picked
    request_count = 0 # Keep track of the number of API requests made
    bad_user = 0 # When calling the riot API, keeps track of how many names we
                 # couldn't find
    regionmap = {}
    rankcount = 0
    rankaggression = {'Iron':{},'Bronze':{},'Silver':{},'Gold':{},'Platinum':{},
        'Diamond':{},'Master':{},'Grandmaster':{},'Challenger':{}}
    rank_arr = ['Iron','Bronze','Silver','Gold','Platinum','Diamond','Master','Grandmaster','Challenger']
    word_map_champion = {}
    word_map_aggression = {}
    word_map_misc = {}
    threelettercount = 0

    # Go through every item in the table
    for row in csvreader:
        # Skip the first line because headers
        if ycount == 0:
            ycount = 1
            continue
        xcount = 0 # Keep track of position by columns in the csv
        for column in row:
            if row[xcount] != "":
                # Perform a lookup in riot's official API to check if the user
                # exists for their given region, reasons this could fail are:
                # 1) user changed their username
                # 2) Troll Data
                # 3) Can't check the region (oversight on our part)
                ''''if xcount == 1:
                    # Parse the line and send the requests
                    regionstring = parse_region(row[xcount+1])
                    if regionstring != "" and column != "":
                        get_return = make_request(regionstring,column,"")
                        if get_return.status_code == 404:
                            print("No summoner found for: %s"%(column,))
                            bad_user += 1
                        request_count += 1
                        if request_count % 100 is 0:
                            print("On request: %d"%(request_count,))
                        #time.sleep(2)'''

                if xcount == 2:
                    if column not in regionmap.keys():
                        regionmap[column] = 1
                    else:
                        regionmap[column] += 1

                # Add primary champion to map, or increment the number of times
                # its been seen as a primary
                if xcount == 3:
                    column = column.rstrip() # Strip the \n
                    # Account for multiple champions in one entry for the
                    # indecisive among us
                    split_list = column.split('/')
                    primarychamp = []
                    # Add the champion to our map and increment the primary
                    # count, while adding it to our current list of primary
                    # champs (which we use later to help understand
                    # the relationship between champions)
                    for element in split_list:
                        if element not in championmap.keys():
                            championmap[element] = 1
                        else:
                            championmap[element] += 1
                        primarychamp.append(element)
                        totalprimarycount += 1

                # Add secondary champion to map or increment the value there
                elif xcount == 5:
                    column = column.rstrip()
                    split_list = column.split('/')
                    # Here we need to say that for every primary champion given,
                    # our user had all of these secondaries, a and b as
                    # primaries will have c and d as secondaries
                    for primary_name in primarychamp:
                        for second_champ in split_list:
                            # Form a temp string to place in our map to
                            # distinguish between secondary and tertiary picks
                            tempstring = primary_name + "secondary"
                            # If the identifier isn't in the relationship map,
                            # Initialize it to a new map
                            if tempstring not in relationshipmap.keys():
                                relationshipmap[tempstring] = {}
                            # Check that the champion is in the identifiers' map
                            # initializing it if it isn't
                            if second_champ not in relationshipmap[tempstring].keys():
                                relationshipmap[tempstring][second_champ] = 1
                            # Otherwise increment the value
                            else:
                                relationshipmap[tempstring][second_champ] += 1
                            # Say we found a secondary
                            totalsecondarycount += 1
                            # This is to keep track of who got chosen as a
                            # secondary, uses more space but that's ok
                            if second_champ not in secondarychampcount.keys():
                                secondarychampcount[second_champ] = 1
                            else:
                                secondarychampcount[second_champ] += 1

                # See elif xcount== 5, follows the same logic as that but with
                # tertiary picks instead
                elif xcount == 7:
                    column = column.rstrip()
                    split_list = column.split('/')
                    for primary_name in primarychamp:
                        for third_champ in split_list:
                            tempstring = primary_name + "tertiary"
                            if tempstring not in relationshipmap.keys():
                                relationshipmap[tempstring] = {}
                            if third_champ not in relationshipmap[tempstring].keys():
                                relationshipmap[tempstring][third_champ] = 1
                            else:
                                relationshipmap[tempstring][third_champ] += 1
                            totaltertiarycount += 1
                            if third_champ not in tertiarychampcount.keys():
                                tertiarychampcount[third_champ] = 1
                            else:
                                tertiarychampcount[third_champ] += 1

                elif xcount == 9:
                    column = column.rstrip()
                    split_list = column.split(' ')
                    for bigword in split_list:
                        word_list = bigword.split('/')
                        for word in word_list:
                            word = remove_numbers(word)
                            word = word.replace('\n','').replace('"','').replace('\f','').replace('«','').replace('_','').replace('•','').replace('[','').replace(']','').replace('“','').replace('(','').replace('<','').replace('>','').replace('^','').replace(':','').replace('=','').replace('?','').replace(')','').replace('\'','').replace('%','').replace('.','').replace('#','').replace('-','').replace('*','').replace('+','').replace('\t','').replace(' ','').rstrip().capitalize()
                            if word.isnumeric() or word == '' or len(word) <= 3:
                                continue
                            if word not in word_map_champion.keys():
                                word_map_champion[word] = 1
                            else:
                                word_map_champion[word] += 1

                # Map the selection of primary champion to the rank the user has
                # supplied us with
                elif xcount == 13:
                    # We need to do this for every primary champion they have
                    # supplied us with, the process is the same for every rank
                    for primary_name in primarychamp:
                        rankmap = parse_rank(column,rankmap,primary_name)

                # Build a map between the level of aggression and the primary
                # champion picks of players
                elif xcount == 14 and column.isnumeric():
                    # Keep track of the values of 1-5
                    if column not in aggressionmap.keys():
                        aggressionmap[column] = 1
                    elif column in aggressionmap.keys():
                        aggressionmap[column] += 1
                    # Now we correlate champion to aggression levels, we can just
                    # do an average because we don't really care about the numbers
                    # we only need to know on average how aggressive a champion
                    # is considered to be
                    for primary_name in primarychamp:
                        if primary_name not in aggressionmap.keys():
                            aggressionmap[primary_name] = [column]
                        else:
                            aggressionmap[primary_name].append(column)
                    for rank in rank_arr:
                        if row[xcount-1] != "" and (rank in row[xcount-1] or rank == row[xcount-1]):
                            if "total" not in rankaggression[rank].keys():
                                rankaggression[rank]["total"] = int(column)
                            else:
                                rankaggression[rank]["total"] += int(column)
                            if "count" not in rankaggression[rank].keys():
                                rankaggression[rank]["count"] = 1
                            else:
                                rankaggression[rank]["count"] += 1
                            break

            xcount += 1
        ycount += 1

    #pprint.pprint(rankaggression)
    #print("Bad user count: %d"%(bad_user,))

    # Output the most common champions without normalizing data
    find_most(rankmap,'RankMapNonNormalized')

    # One way of normalizing
    rankmap = normalizedata(rankmap)

    # Now we print out the results of z-score normalization
    #pprint.pprint(z_score_normalizing(rankmap))

    # Sort the (now normalized) rank map
    find_most(rankmap,'RankMapNormalized')

    statfile = open('results/champion_explanation',"w")


    removeFiller(word_map_champion,championmap)
    #pprint.pprint(sorted(word_map_champion.items(), key = lambda kv:(kv[1], kv[0]),reverse=True))
    for (word,count) in sorted(word_map_champion.items(), key = lambda kv:(kv[1], kv[0]),reverse=True):
        if count > 2:
            statfile.write('%s\t%d\n'%(word,count))

    statfile.close()

    statfile = open('results/MainChampionPercentages',"w")

    # Write out the percentages of play relative and the total numer of picks
    for key,value in championmap.items():
        statfile.write("%s\t\t%d\t\t%.2f\n"%(key[:len(key)],value,(value / totalprimarycount) * 100))
        #print("%s had %d votes for primary, or %f of total"%(key,value,(value / totalprimarycount)))

    statfile.close()

    statfile = open('results/rankAggression',"w")

    # Write out the percentages of play relative and the total numer of picks
    for rank,value in rankaggression.items():
        if rank == "Iron":
            statfile.write("%s\t%.2f\n"%("Iron and Bronze",(rankaggression["Iron"]["total"] + rankaggression["Bronze"]["total"])/(rankaggression["Iron"]["count"] + rankaggression["Bronze"]["count"])))
        elif rank== "Master":
            statfile.write("%s\t%.2f\n"%("Master, Grandmaster, Challenger",(rankaggression["Master"]["total"] + rankaggression["Grandmaster"]["total"]+ rankaggression["Challenger"]["total"])/(rankaggression["Master"]["count"] + rankaggression["Grandmaster"]["count"]+ rankaggression["Challenger"]["count"])))
        else:
            statfile.write("%s\t%.2f\n"%(rank,(rankaggression[rank]["total"] / rankaggression[rank]["count"])))
        #print("%s had %d votes for primary, or %f of total"%(key,value,(value / totalprimarycount)))

    statfile.close()

    statfile = open("results/SecondaryChampionPercentages","w")

    for key,value in secondarychampcount.items():
        statfile.write("%s\t\t%d\t\t%.2f\n"%(key[:len(key)],value,(value / totalsecondarycount) * 100))

    statfile.close()

    statfile = open("results/TertiaryChampionPercentages","w")

    for key,value in tertiarychampcount.items():
        statfile.write("%s\t\t%d\t\t%.2f\n"%(key[:len(key)],value,(value / totaltertiarycount) * 100))

    statfile.close()

    statfile = open("results/regionmap",'w+')

    for key,value in regionmap.items():
        statfile.write("%s\t%d\n"%(key,value))

    statfile.close()

    statfile = open("results/Connections","w")

    for key in relationshipmap:
        if 'secondary' in key:
            champ_name = key.replace('secondary','')
            statfile.write("%s\tSecondaries\n"%(champ_name,))
        elif 'tertiary' in key:
            champ_name = key.replace('tertiary','')
            statfile.write("%s\tTertiaries\n"%(champ_name,))
        newlist = countdict(relationshipmap[key])
        for (champion,count) in newlist:
            statfile.write("\t%d\t%s\n"%(count,champion))
        statfile.write("\n")
        #print("%s had the following connections: %s"%(key,countdict(relationshipmap[key])))

    statfile.close()

    statfile = open("results/Aggression","w")
    secondstatfile = open("results/Playeraggression","w")

    # Now we get the aggression levels of each champion
    for key in aggressionmap:
        agvalue = 0
        # If our key is not a number (used later), get the average per champion
        if not key.isnumeric() and key != "":
            for value in aggressionmap[key]:
                agvalue += int(value)
            statfile.write("%s\t%.2f\n"%(key,(agvalue / (len(aggressionmap[key])))))

    # Go 1 - 5 and write out the level of aggression and what percentage that
    # represents
    for i in range(1,6):
        secondstatfile.write('Level %d aggression: %s out of %d:\t%.2f%%\n'%(i,aggressionmap[str(i)],ycount,100 *aggressionmap[str(i)] / ycount))

    statfile.close()
    secondstatfile.close()

    return

# Function to sort the list of tuples by its second item in reverse order
# most-->least
def Sort_Tuple(tup):

    # reverse = None (Sorts in Ascending order)
    # key is set to sort using second element of
    # sublist lambda has been used
    tup.sort(key = lambda x: x[1],reverse=True)
    return tup

# Build and order our relationship list to show which picks correlated to which
# secondary and tertiary picks, denoted as relationship maps
def countdict(ourdict):
    relationlist = []
    for champ in ourdict:
        relationlist.append((champ,ourdict.get(champ)))
    #return relationlist.sort(key=sortSecond)
    return Sort_Tuple(relationlist)

def main():
    # Reading file
    csvfile = open('League of Legends Ranked Champion Selection Survey (Responses) - Form Responses 1.csv', 'r')
    championlistfile = open(r'champcopy',"r")

    # Read in the file and make up a champion list
    championslist = []
    for line in championlistfile:
        championslist.append(line.upper().rstrip())
    championlistfile.close()

    # Create a csv reader object
    csvreader = csv.reader(csvfile, delimiter=',')

    check_file(championslist,csvreader)

    csvfile.close()

    csvfile1 = open('League of Legends Ranked Champion Selection Survey (Responses) - Form Responses 1.csv', 'r')
    # Create a csv reader object
    csvreader1 = csv.reader(csvfile1, delimiter=',')

    pullStatistics(championslist,csvreader1)

    csvfile.close()

    return

if __name__ == "__main__":
    main()
