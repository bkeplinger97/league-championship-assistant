import csv
# pip3 install pprint
# Makes things print out pretty
import pprint

# The open file commands in case we needed them
#with open('csvfile','r') as file:
    #csvreader1 = csv.reader(file, delimiter=',')


'''we need to look at the following columns (E, G, I) in order to determine the roles played. This function will read the CSV and then assign it to sections in the defined dictionary d. The columns E G and I correspond to the number 5, 7, and 9'''
def skimmer(csv_file):
    d = {} # d for dictionary
    row_counter = 0 # Y variable
    col_counter = 0 # X variable
    champion_name = ''
    for row in csv_file:
        if row_counter == 0:
            row_counter += 1
            continue
        for item in row:
            item = item.rstrip() # Strips new line character
            if col_counter == 3 and item != "":
                champion_name = item
                if item not in d.keys():
                    d[champion_name] = {'Bottom':0,'Top':0,'Jungle':0,'Support':0,'Middle':0}
            elif col_counter == 4 and item != "":
                d[champion_name][item] += 1 # Item is the role
            elif col_counter == 5 and item != "":
                champion_name = item
                if item not in d.keys():
                    d[champion_name] = {'Bottom':0,'Top':0,'Jungle':0,'Support':0,'Middle':0}
            elif col_counter == 6 and item != "":
                d[champion_name][item] += 1 # Item is the role
            elif col_counter == 7 and item != "":
                champion_name = item
                if item not in d.keys():
                    d[champion_name] = {'Bottom':0,'Top':0,'Jungle':0,'Support':0,'Middle':0}
            elif col_counter == 8 and item != "":
                d[champion_name][item] += 1 # Item is the role
            col_counter += 1
        col_counter = 0
        row_counter += 1
    return d

def main(): # writing main function
    with open('League of Legends Ranked Champion Selection Survey (Responses) - Form Responses 1.csv','r') as file: # Replace with name of your file
        csvr = csv.reader(file, delimiter=',')
        pprint.pprint(skimmer(csvr))
    # does not return

if __name__ == "__main__":
    main()
    # does not return
