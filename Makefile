CC = rustc
CFLAGS = -Wall -Isrc -I.

all: bin/championparser #bin/server

bin/championparser:
	rustc src/championparser.rs

bin/server:
	$(CC) src/server.rs

clean:
	rm -f Aggression Connections Playeraggression MainChampionPercentages* SecondaryChampionPercentages TertiaryChampionPercentages
